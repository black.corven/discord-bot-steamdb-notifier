import {Bot} from "./bot";
import 'dotenv/config'
import * as fs from "fs";

const configPath = './config.json';
if(fs.existsSync(configPath))
{
    const app_config = JSON.parse(fs.readFileSync(configPath).toString());
    if(app_config) {
        const bot = new Bot(
            app_config.steamAppId,
            app_config.steamAppBranchName,
            app_config.channelId,
            app_config.roleId,
            app_config.requestIntervalMs
        );

        bot.listen(process.env.BOT_TOKEN!, app_config.useSteamDBRealtimeApi)
            .catch((reason) => {
                console.log("Could not connect!");
                console.error(reason);
            });
    } else {
        console.log("Can't parse file");
    }
} else {
    console.log("No config file!");
}


