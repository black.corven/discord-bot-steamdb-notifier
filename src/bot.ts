import {Client, MessageEmbed, TextChannel} from 'discord.js';
import {fetchBranchInfo, listenRealtimeMonitor} from "./steamdb_branch_scraper";
import * as fs from 'fs';

const timestampFile = './timestamp.json';

export class Bot
{
    private appId:string = "";
    private branchName:string = "";
    private channelId:string = "";
    private roleId:string = "";
    private updateInterval: number = 10 * 60 * 1000;

    private lastTimestamp:number = 0;

    constructor(appId:string,
                branchName:string,
                channelId: string,
                roleId: string,
                interval: number)
    {
        this.appId = appId;
        this.branchName = branchName;
        this.channelId = channelId;
        this.roleId = roleId;
        this.updateInterval = interval;

        // Load timestamp from file
        try {
            if(fs.existsSync(timestampFile))
            {
                const file = JSON.parse(fs.readFileSync(timestampFile).toString());
                if(file) {
                    this.lastTimestamp = file.lastTimestamp;
                }
            }
        } catch(err) {
            console.error(err)
        }

    }

    private async checkAppBranch(client: Client)
    {
        const channel = <TextChannel> await client.channels.fetch(this.channelId);
        const branchInfo : any = await fetchBranchInfo(this.appId, this.branchName);
        if(0 === branchInfo.newTimestamp)
        {
            console.error("Can't fetch data from SteamDB.");
            return;
        }
        if(this.lastTimestamp === 0) {
            this.lastTimestamp = branchInfo.newTimestamp;
            console.log(`Set initial timestamp by request: ${this.lastTimestamp}`);
            fs.writeFileSync(timestampFile, JSON.stringify({ lastTimestamp: this.lastTimestamp }, null, 2));
            return;
        }
        if(this.lastTimestamp < branchInfo.newTimestamp)
        {
            this.lastTimestamp = branchInfo.newTimestamp;
            fs.writeFileSync(timestampFile, JSON.stringify({ lastTimestamp: this.lastTimestamp }, null, 2));

            //await channel.send(`<@&${this.roleId}> Catch an update of rdlc branch at ${new Date(branchInfo.newTimestamp).toUTCString()}`);
            await channel.send({
                content: '',
                    embed: new MessageEmbed()
                .setColor('#0099ff')
                .setAuthor(
                    'SteamDB',
                    'https://steamdb.info/static/logos/512px.png',
                    `https://steamdb.info/app/${this.appId}/depots/?branch=${this.branchName}`)
                .setDescription((this.roleId !== "0")? `<@&${this.roleId}> ` : `` + `Catch an update.`)
                .setTimestamp(new Date(branchInfo.newTimestamp))
            });
        }
    }

    public listen(token:string, mode:boolean): Promise<string>
    {
        const client = new Client();
        client.once("ready", async () =>
        {
            console.log(`Bot is connected as ${client.user?.tag}!`);

            const thread = await client.channels.fetch(this.channelId);

            if(thread === null) {
                console.error("No such channel!");
                return;
            }
            if(!thread.isText()) {
                console.error("Chosen channel must be text channel.");
                return;
            }

            // Check branch first time
            this.checkAppBranch(client).then(() => {
                if (mode) {
                    // Wanna listen steam.db/realtime WebSocket
                    listenRealtimeMonitor(this.appId, () => {
                        this.checkAppBranch(client);
                    });
                } else {
                    // Wanna check branch manually
                    setInterval(() => {
                        this.checkAppBranch(client);
                    }, this.updateInterval);
                }
            });
        });

        return client.login(token);
    }
}
