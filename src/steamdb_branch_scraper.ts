import WebSocket from 'ws';
import * as https from 'https';

const wssURL = 'wss://steamdb.info/api/realtime/';
const rePart1 = `<li><span class="branch-name">`;
const rePart2 = `<\\/span>\\s<i>timeupdated:<\\/i>\\s<del>(\\d+)<\\/del>\\s<i class="muted">\\([\\w \\d–\\-:]+\\)<\\/i>\\s&rsaquo;\\s<ins>(\\d+)<\\/ins>\\s<i class="muted">\\([\\w \\d–\\-:]+\\)<\\/i><\\/li>`;

const optionsGetById = {
    hostname: "steamdb.info",
    port: 443,
    path: "/api/GetAppHistory/",
    method: 'GET',
    TE: "trailers",
    Connection: "keep-alive",
    secureProtocol: "TLSv1_2_method", // sic!
    timeout: 15000,
    "headers": {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0",
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "X-Requested-With": "XMLHttpRequest",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "no-cors",
        "Sec-Fetch-Site": "same-origin",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
    },
    //Cookie:
    //`cf_clearance=${cf_clearance}; __Host-steamdb={steamdb_coockie}}`
};

export function listenRealtimeMonitor(appId:string, callback: () => void)
{
    var socket = new WebSocket(wssURL);
    socket.onclose = function(event)
    {
        if (event.wasClean) {
            console.log('WebSocket connection closed');
        } else {
            console.log('WebSocket connection lost'); // server issue etc
        }
        console.log(event.code + ': ' + event.reason);
    };

    socket.onmessage = function(event)
    {
        if (typeof event.data !== "string") { return; }
        const obj: any = JSON.parse(event.data);
        if (!obj || obj.Users) return;
        if (obj.Apps[appId] !== undefined) {
            callback();     // Callback if message contains specified appId
        }
    };

    socket.onerror = function(error)
    {
        console.error("WebSocket Error: " + error);
    };
}

export function fetchBranchInfo(appId:string, branchName:string, cf_cookie?:string)
{
    return new Promise<object>((resolve, reject) => {
        const tmpOptions  = Object.assign({}, optionsGetById);
        if(cf_cookie) {
            Object.assign(tmpOptions, {
                Cookie: `cf_clearance=${cf_cookie}`
            });
        }
        tmpOptions.path = tmpOptions.path + `?lastentry=0&appid=${appId}`;

        const reBranchTimestamps = new RegExp(rePart1 + branchName + rePart2, "g");

        const req = https.request(tmpOptions, (res) => {
            let body = '';
            res.on('data', d => {
                body += d;
            });
            res.on('end', () => {
                let tmp = reBranchTimestamps.exec(body);
                if(tmp) {
                    resolve({
                        newTimestamp: tmp.pop(),
                        lastTimestamp: tmp.pop()
                    });
                } else {
                    resolve({ newTimestamp: 0 });
                }
            });
        });

        req.on('error', error => {
            if(error.name === 'ETIMEDOUT')
            {
                reject('Timeout');
            } else {
                reject(error);
            }
        });
        req.end();
    });
}
