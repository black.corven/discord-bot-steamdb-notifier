# Discord Bot - SteamDB Notifier

## Description
A Discord Bot that notifies users by role as soon as a specific branch's changes are posted in the SteamDB WebSocket feed.  
WARNING: The SteamDB frontend API is used to get detailed information about a branch.  

July 2021

## Install
1. Get Discord Bot Token and put in `.env`
2. Fill config.json
3. Install node.js
4. From project dir install dependencies `install_dev.bat`
5. Run app by script `nmp run start` or by file `start.bat`

## Config
config.json parameters:
* `useSteamDBRealtimeApi` - true: uses WebSocket feed, false: requests the last change from the app page with interval
* `channelId` - ID of the discord channel where notifications are sent
* `roleId` - ID of the discord role to be notified
* `steamAppId` - Application Id in SteamDB
* `steamAppBranchName` - Branch's name in that application
* `requestIntervalMs` - timeout beetween requests if `useSteamDBRealtimeApi = false`

## Acknowledgment
Made at [Asdolg](https://github.com/AsdolgTheMaker)'s request.

## Project status
Development stopped until next request.

## License
MIT
